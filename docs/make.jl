using VDJsim
using Documenter

DocMeta.setdocmeta!(VDJsim, :DocTestSetup, :(using VDJsim); recursive=true)

makedocs(;
    modules=[VDJsim],
    authors="Mateusz Kaduk <mateusz.kaduk@gmail.com> and contributors",
    repo="https://gitlab.com/mateusz-kaduk/vdjsim.jl/blob/{commit}{path}#{line}",
    sitename="VDJsim.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://mateusz-kaduk.gitlab.io/VDJsim.jl",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
