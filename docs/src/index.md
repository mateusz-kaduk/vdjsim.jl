```@meta
CurrentModule = VDJsim
```

# VDJsim

Documentation for [VDJsim](https://gitlab.com/mateusz-kaduk/vdjsim.jl).

# Usage

Example use case is to generate dataframe with simulated rearrange VDJs and their mask.
Where `n` corresponds to repertoire size (number of reads).
```
using VDJsim

Vs = VDJsim.load_fasta("data/Macaca-mulatta_Ig_Heavy_V_1-1.fasta")
Ds = VDJsim.load_fasta("data/Macaca-mulatta_Ig_Heavy_D_1-1.fasta")
Js = VDJsim.load_fasta("data/Macaca-mulatta_Ig_Heavy_J_1-1.fasta")

df = VDJsim.simulate_VDJ(Vs, Ds, Js;n=400)
```

# API

```@autodocs
Modules = [VDJsim]
```
