module VDJsim
    using StatsBase
    using Distributions
    using FASTX
    using BioSequences
    using DataFrames
    using ProgressMeter
    using Random
    export load_fasta, simulate_VDJ, simulate_VDJ_parallel, random_sequence, translate_to_aa
    export Parameters, VarLenPoisson

    struct Parameters
        samples::Int
        trim_length_dist::Distribution
        pali_length_dist::Distribution
        nont_length_dist::Distribution
        mutation_rate_dist::Distribution
        productive_rate_dist::Distribution
        flank_length_dist::Distribution
        flank_length_min::Int
        insert_rate_dist::Distribution
        delete_rate_dist::Distribution
        indel_size_dist::NamedTuple{(:tw, :td, :sd, :cap), Tuple{Distribution, Distribution, Distribution, Int}}
        pairing_prior::Distribution
        pairing_cap::Int
    end
    function Parameters(;samples,trim_length_dist, pali_length_dist, nont_length_dist,
        mutation_rate_dist, productive_rate_dist, flank_length_dist, flank_length_min,
        insert_rate_dist, delete_rate_dist, indel_size_dist, pairing_prior, pairing_cap)
        Parameters(samples, trim_length_dist, pali_length_dist, nont_length_dist,
        mutation_rate_dist, productive_rate_dist, flank_length_dist,flank_length_min,
        insert_rate_dist, delete_rate_dist, indel_size_dist, pairing_prior, pairing_cap)
    end

    """
    Sample the length of trimming
    """
    function trim_draw(d::Distribution)
        return rand(d)
    end

    """
    Sample the length of palindrome
    """
    function p_draw(d::Distribution)
        return rand(d)
    end

    """
    Sample the length of non-templated `x ∼ Poisson(p)`
    """
    function n_draw(d::Distribution)
        return rand(d)
    end

    """
    Return a default mask
    """
    function mask_from_seq(s, m)
        return m^length(s)
    end

    """
    Kadane's algorithm for finding a subarray with maximum sum
    """
    function max_subarray(arr::Vector{<:Number})
        largest_ending_here = 0
        best_start = this_start = ending = best_so_far = 1
        for (i,x) in enumerate(arr)
            largest_ending_here += x
            best_so_far = max( best_so_far, largest_ending_here )
            if largest_ending_here <= 0
                this_start = i + 1
                largest_ending_here = 0
            elseif largest_ending_here == best_so_far
                best_start = this_start
                ending = i
            end
        end
        return best_so_far, best_start, ending
    end

    """
    Prior to encourage pairing to be near the ends
    """
    function merge_prior(n::Int, d::Distribution)
        return logpdf(d,n)
    end

    """
    Score given paring
    """
    function pairing_score(l_end,r_start; match_K = 5.0, mismatch_K = -100.0)
        matches = 0.0 .+ (collect(l_end) .== collect(r_start))
        matches .+= ((1 .- matches) .* mismatch_K) .+ (matches .* match_K)
        return max_subarray(matches)
    end

    """
    Check all pairings from two sequences and score them to find best one
    """
    function snap(left, right, d::Distribution, cap)
        cap = min(cap, min(length(left)), min(length(right)))
        scores = [pairing_score(left[end-i+1:end], right[1:i])[1] + merge_prior(i, d) for i in 1:cap]
        scores = scores .- maximum(scores)
        scores = ℯ .^ scores
        scores = scores ./ sum(scores)
        pairpos = sample(1:cap, Weights(scores))
        _, s, e = pairing_score(left[end-pairpos+1:end], right[1:pairpos])
        return pairpos, s, e
    end

    """
    Merge rearrangment sequences and masks
    """
    function merge_seqs(left, right, left_mask, right_mask, param::Parameters)
        left_trim,right_trim = trim_draw(param.trim_length_dist),trim_draw(param.trim_length_dist)
        left = left[1:end-left_trim]
        left_mask = left_mask[1:end-left_trim]
        right = right[1+right_trim:end]
        right_mask = right_mask[1+right_trim:end]

        left_p,right_p = p_draw(param.pali_length_dist),p_draw(param.pali_length_dist)
        var = left[end-left_p+1:end]
        left = left*revcomp(left[end-left_p+1:end])
        left_mask = left_mask*"P"^left_p
        right = revcomp(right[1:right_p])*right
        right_mask = ("P"^right_p)*right_mask

        left_n,right_n = n_draw(param.nont_length_dist),n_draw(param.nont_length_dist)
        left = left*join(rand(['A','C','G','T'],left_n))
        left_mask = left_mask*"N"^left_n
        right = join(rand(['A','C','G','T'],right_n))*right
        right_mask = ("N"^right_n)*right_mask

        shift,from,to = snap(left, right, param.pairing_prior, param.pairing_cap)
        #Assuming that sequences in the "matched" region will be identical.
        #Should be near-universally true with our current penalties.
        #If that is sometimes false, then this will just take from the "left".
        #Should maybe just randomly choose which strand to keep on?
        merged = left[1:end-shift+to]*right[to+1:end] #Loads of bounds checking went into this...
        merged_mask = left_mask[1:end-shift+to]*right_mask[to+1:end]
        return merged,merged_mask
    end

    """
    Reverse complement a string
    """
    function revcomp(seq)
        join(string.(reverse_complement(LongDNA{4}(seq))))
    end

    """
    Sequentially rearrange V D and J
    """
    function rearrange(V, D, J, param::Parameters)
        DJ,DJ_mask = merge_seqs(D,J,mask_from_seq(D,"D"), mask_from_seq(J,"J"), param)
        VDJ, VDJ_mask = merge_seqs(V,DJ,mask_from_seq(V,"V"), DJ_mask, param)
        return VDJ, VDJ_mask
    end

    """
    Rearrange V and J only
    """
    function rearrange(V,J, param::Parameters)
        VJ,VJ_mask = merge_seqs(V,J,mask_from_seq(V,"V"),mask_from_seq(J,"J"), param)
        return VJ,VJ_mask
    end

    """
    Determine indel size
    """
    function indel_size(tw::Distribution, td::Distribution, sd::Distribution, cap::Int)
        mod(ifelse(rand(tw), (rand(td))*3+2, rand(sd)), cap) + 1
    end

    """
    Determine mutation rate
    """
    function mutation_rate(d::Distribution)
        return rand(d::Distribution)
    end

    """
    Insertion
    """
    function str_insert(seq::String,ins::String,pos::Int)
        return seq[1:pos]*ins*seq[pos+1:end]
    end

    """
    Deletion
    """
    function str_delete(seq::String,size::Int,pos::Int)
        return seq[1:pos]*seq[pos+size+1:end]
    end

    """
    Insertion for both sequence and a mask
    """
    function masked_insertion(seq,mask, param::Parameters)
        size = indel_size(param.indel_size_dist...)
        pos = rand(1:length(seq))
        #Stopgap for now: This should instead sometimes duplicate the sequence either
        #side of the indel position and sometimes insert random characters like this.
        ins = join(rand(['A','C','G','T'],size))
        return str_insert(seq,ins,pos), str_insert(mask,"I"^size,pos)
    end

    """
    Deletion for both sequence and a mask
    """
    function masked_deletion(seq,mask, param::Parameters)
        size = indel_size(param.indel_size_dist...)
        pos = rand(1:length(seq))
        return str_delete(seq,size,pos), str_delete(mask,size,pos)
    end

    """
    Mutate sequence at `prop` rate
    """
    function mutate(seq::String, prop::Float64)
        #Should likely build in some SHM hotspots in here.
        join([ifelse(rand()<prop,rand(['A','C','G','T']),c) for c in seq])
    end

    """
    Function to load regular FASTA file
    """
    function load_fasta(path)
        records = []
        open(FASTA.Reader, path) do reader
            for record in reader
                push!(records, (FASTA.identifier(record), string(FASTA.sequence(record))))
            end
        end
        return records
    end

    """
    Translate nucleotides
    """
    function translate_to_aa(ab)
        join(string.(translate(LongDNA{4}(ab[1:3*Int(floor(length(ab)/3))]))))
    end

    """
    Function to recover coordinates from the mask.
    The assumption is that simlated mask is ground truth so uninterrupted contigouse regions.
    """
    function coordinate(mask)
        v_start = findfirst('V', mask)
        v_end = findlast('V', mask)
        j_start = findfirst('J', mask)
        j_end = findlast('J', mask)
        d_start = findfirst('D', mask)
        d_end = findlast('D', mask)
        if (j_start === nothing) || (j_end === nothing)
            @info mask
        end
        return v_start, v_end, d_start, d_end, j_start, j_end
    end

    function random_sequence(d::Distribution; minlen::Int=0)
        join(rand("ATGC",rand(d)+minlen))
    end

    """
        simulate_single_precursor(Vs, Ds, Js)

    Simulate a single rearranged VDJ and its mask.
    """
    function simulate_single_precursor(Vs, Ds, Js, param::Parameters)
        Vind = rand(1:length(Vs))
        Dind = rand(1:length(Ds))
        Jind = rand(1:length(Js))
        V = Vs[Vind][2]
        D = Ds[Dind][2]
        J = Js[Jind][2]
        Vname = Vs[Vind][1]
        Dname = Ds[Dind][1]
        Jname = Js[Jind][1]

        ab, mask = rearrange(V, D, J, param)

        ins = rand(param.insert_rate_dist)
        if ins > 0
            ab, mask = masked_insertion(ab, mask, param)
        end
        del = rand(param.delete_rate_dist)
        if del > 0
            ab, mask = masked_deletion(ab, mask, param)
        end
        ab = mutate(ab, mutation_rate(param.mutation_rate_dist))

        nonfunctional = (mod(length(ab), 3) != 0) || occursin("*", translate_to_aa(ab[1:3*Int(floor(length(ab)/3))]))

        # Trim terminal Ns that were just being used to decide on the J reading frame
        real_end = findlast(collect(ab) .!= 'N')
        ab = ab[1:real_end]
        mask = mask[1:real_end]

        # Flank
        if param.flank_length_dist !== nothing
            prefix = random_sequence(param.flank_length_dist, minlen=param.flank_length_min)
            suffix = random_sequence(param.flank_length_dist, minlen=param.flank_length_min)
            mask = "B"^length(prefix)*mask*"E"^length(suffix)
            ab = prefix*ab*suffix
        end

        vstart, vend, dstart, dend, jstart, jend = coordinate(mask)

        return ab, mask, Vname, Dname, Jname, !nonfunctional, del, ins, vstart, vend, dstart, dend, jstart, jend
    end

    """
    Simulate `n` rearranged VDJ and their mask.
    """
    function simulate_VDJ(Vs, Ds, Js, param::Parameters)
        df = DataFrame(sequence = String[],
                       mask = String[],
                       v_call = String[],
                       d_call = String[],
                       j_call = String[],
                       productive = Bool[],
                       deletion = Bool[],
                       insertion = Bool[],
                       v_sequence_start = Int[],
                       v_sequence_end = Int[],
                       d_sequence_start = Vector{Union{Nothing, Int}}(),
                       d_sequence_end = Vector{Union{Nothing, Int}}(),
                       j_sequence_start = Int[],
                       j_sequence_end = Int[]);
        pushed = 0
        generated = 0
        p = Progress(param.samples, desc="Simulating rearranged VDJs: ", showspeed=true)
        while (pushed < param.samples)
            generated += 1

            ab, mask, Vname, Dname, Jname, productive, del, ins, vstart, vend, dstart, dend, jstart, jend = simulate_single_precursor(Vs, Ds, Js, param)

            to_push = true
            #Only push some of the time when nonfunctional.
            #Note this doens't directly control the ratio!
            if (!productive) && rand(param.productive_rate_dist)
                to_push = false
            end
            if to_push
                vstart, vend, dstart, dend, jstart, jend = coordinate(mask)
                push!(df, [ab, mask, Vname, Dname, Jname, productive, del, ins, vstart, vend, dstart, dend, jstart, jend])
                pushed += 1
                # Update progress bar
                next!(p)
            end
        end
        return df
    end

    """
        simulate_VDJ_parallel(Vs, Ds, Js; threads=Threads.nthreads())

    Simulate `n` rearranged VDJ and their mask in parallel.
    """
    function simulate_VDJ_parallel(Vs, Ds, Js, param::Parameters; threads=Threads.nthreads())
        p = Progress(param.samples, desc="Simulating rearranged VDJs: ", showspeed=true)
        dfs = [DataFrame(sequence = String[],
                         mask = String[],
                         v_call = String[],
                         d_call = String[],
                         j_call = String[],
                         productive = Bool[],
                         deletion = Bool[],
                         insertion = Bool[],
                         v_sequence_start = Int[],
                         v_sequence_end = Int[],
                         d_sequence_start = Vector{Union{Nothing, Int}}(),
                         d_sequence_end = Vector{Union{Nothing, Int}}(),
                         j_sequence_start = Int[],
                         j_sequence_end = Int[]) for _ in 1:Threads.nthreads()]

        total_pushed = Threads.Atomic{Int}(0)

        while total_pushed[] < param.samples
            Threads.@threads for _ in 1:threads
                if total_pushed[] >= param.samples
                    break
                end

                ab, mask, Vname, Dname, Jname, productive, del, ins, vstart, vend, dstart, dend, jstart, jend = simulate_single_precursor(Vs, Ds, Js, param)
                if (!productive) && rand(param.productive_rate_dist)
                    continue
                end
                tid = Threads.threadid()
                push!(dfs[tid], (ab, mask, Vname, Dname, Jname, productive, del, ins, vstart, vend, dstart, dend, jstart, jend))
                Threads.atomic_add!(total_pushed, 1)
                next!(p)  # Update progress bar; Not strictly thread-safe
            end
        end

        df = vcat(dfs...)  # Concatenate all thread-specific DataFrames into a single DataFrame
        return df
    end
end
