include("VDJsim.jl")
using .VDJsim
using StatsBase
using Distributions
using FASTX
using BioSequences
using DataFrames
using ProgressMeter
using CSV
using Distributions

Vs = VDJsim.load_fasta("data/Macaca-mulatta_Ig_Heavy_V_1-1.fasta")
Ds = VDJsim.load_fasta("data/Macaca-mulatta_Ig_Heavy_D_1-1.fasta")
Js = VDJsim.load_fasta("data/Macaca-mulatta_Ig_Heavy_J_1-1.fasta")

param = Parameters(
    samples=5000000,
    trim_length_dist=Poisson(0.8),
    pali_length_dist=Poisson(0.5),
    nont_length_dist=Poisson(0.4),
    mutation_rate_dist=Exponential(0.075),
    productive_rate_dist=Bernoulli(0.95),
    flank_length_dist=NegativeBinomial(50, 0.5),
    flank_length_min=25,
    insert_rate_dist=Bernoulli(0.1),
    delete_rate_dist=Bernoulli(0.1),
    indel_size_dist=(tw=Bernoulli(0.9), td=Geometric(0.7), sd=Geometric(0.15), cap=15),
    pairing_prior=Geometric(0.5),
    pairing_cap=15
    )

df = simulate_VDJ_parallel(Vs, Ds, Js, param)
CSV.write("VDJsim-01-09-2023.tsv.gz", df, delim='\t', transform=(col, val) -> something(val, missing), compress=true)
